
package anggaran.view;

import javafx.fxml.FXML;
import anggaran.model.Anggaran;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AnggaranEditController {
    @FXML
    private TextField namaField;
    @FXML
    private TextArea deskripsiField;
    @FXML
    private Stage dialogStage;
    @FXML
    private Anggaran anggaran;
    
    private boolean saveClicked = false;
    
    @FXML
    private void initialize() {}
    
    public void setDialogStage(Stage dialog) {
        dialogStage = dialog;
    }
    
    public void setAnggaran(Anggaran anggaran) {
        this.anggaran = anggaran;
        
        namaField.setText(anggaran.getNama());
        deskripsiField.setText(anggaran.getDeskripsi());
    }
    
    public void cancelHandler() {
        dialogStage.close();
    }
    
    public void saveHandler() {
        anggaran.setNama(namaField.getText());
        anggaran.setDeskripsi(deskripsiField.getText());
        saveClicked = true;
        dialogStage.close();
    }
    
    public boolean isSaveClicked() {
        return saveClicked;
    }
    
    
}
