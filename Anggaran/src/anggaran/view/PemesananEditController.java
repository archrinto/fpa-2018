
package anggaran.view;

import anggaran.model.Anggaran;
import anggaran.model.Divisi;
import javafx.fxml.FXML;
import anggaran.model.Pemesanan;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class PemesananEditController {
    @FXML
    private ComboBox<Divisi> divisiComboBox;
    @FXML
    private DatePicker tanggalDatePicker;
    @FXML
    private ComboBox<Anggaran> anggaranComboBox;
    @FXML 
    private TextField nominalField;
    @FXML
    private ComboBox statusComboBox;
    @FXML
    private Stage dialogStage;
    @FXML
    private Pemesanan pemesanan;
    
    private DivisiController divisiController;
    
    private AnggaranController anggaranController;
    
    private boolean saveClicked = false;
    
    @FXML
    private void initialize() {
        divisiController = new DivisiController();
        divisiController.loadDivisiDataFromFile();
        divisiComboBox.setItems(divisiController.getDivisiData());
        
        anggaranController = new AnggaranController();
        anggaranController.loadAnggaranDataFromFile();
        anggaranComboBox.setItems(anggaranController.getAnggaranData());
        
        String pattern = "yyyy-MM-dd";
        tanggalDatePicker.setPromptText(pattern.toLowerCase());
        tanggalDatePicker.setConverter(new StringConverter<LocalDate>() {
             DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

             @Override 
             public String toString(LocalDate date) {
                 if (date != null) {
                     return dateFormatter.format(date);
                 } else {
                     return "";
                 }
             }

             @Override 
             public LocalDate fromString(String string) {
                 if (string != null && !string.isEmpty()) {
                     return LocalDate.parse(string, dateFormatter);
                 } else {
                     return null;
                 }
             }
        });
    }
    
    public void setDialogStage(Stage dialog) {
        dialogStage = dialog;
    }
    
    public void setPemesanan(Pemesanan pemesanan) {
        this.pemesanan = pemesanan;
        divisiComboBox.setValue(pemesanan.getDivisi());
        anggaranComboBox.setValue(pemesanan.getAnggaran());
        nominalField.setText(Integer.toString(pemesanan.getNominal()));
        tanggalDatePicker.setValue(pemesanan.getTanggal());
    }
    
    public void cancelHandler() {
        dialogStage.close();
    }
    
    public void saveHandler() {
        pemesanan.setDivisi(divisiComboBox.getSelectionModel().getSelectedItem());
        pemesanan.setAnggaran(anggaranComboBox.getSelectionModel().getSelectedItem());
        pemesanan.setNominal(new Integer(nominalField.getText()));
        pemesanan.setTanggal(tanggalDatePicker.getValue());
        pemesanan.setStatus("Diajukan");
        saveClicked = true;
        dialogStage.close();
    }
    
    public boolean isSaveClicked() {
        return saveClicked;
    }
    
    
}
