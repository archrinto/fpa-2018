package anggaran.view;

import anggaran.MainApp;
import anggaran.model.Anggaran;
import anggaran.model.Divisi;
import anggaran.model.Pemesanan;
import anggaran.model.PemesananListWrapper;
import anggaran.model.Persetujuan;
import anggaran.model.User;
import java.io.File;
import java.time.LocalDate;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


public class PersetujuanController {
    
    private ObservableList<Pemesanan> persetujuanData = FXCollections.observableArrayList();
    
    private MainApp mainApp;
    private PemesananController pemesananController;
    private User user;
    
    @FXML
    private TableView<Pemesanan> persetujuanTable;
    @FXML
    private TableColumn<Pemesanan, Divisi> divisiColumn;
    @FXML
    private TableColumn<Pemesanan, Anggaran> anggaranColumn;
    @FXML
    private TableColumn<Pemesanan, Number> nominalColumn;
    @FXML
    private TableColumn<Pemesanan, LocalDate> tanggalColumn;
    @FXML
    private Button persetujuanButton;
    @FXML
    private ComboBox<Divisi> divisiComboBox;
    @FXML
    private ComboBox<Anggaran> anggaranComboBox;
   
    
    public void initialize() {
        pemesananController = new PemesananController();
        pemesananController.loadPemesananDataFromFile();
                
        divisiColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().divisiProperty();
        });
        anggaranColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().anggaranProperty();
        });
        nominalColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().nominalProperty();
        });
        tanggalColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().tanggalProperty();
        });
               
        persetujuanTable.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> enableButtonAction());
    }
    
    
    public void showPersetujuanDialog(Pemesanan pemesanan) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("PersetujuanDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            
            PersetujuanDialogController controller = loader.getController();
            
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Persetujuan");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(mainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            controller.setPemesanan(pemesanan);
            controller.setDialogStage(dialogStage);
            
            dialogStage.showAndWait();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    } 
    
    public void refreshTable() {
        pemesananController.loadPemesananDataFromFile();
        setPersetujuanData(pemesananController.getPemesananData());
    }
    
    public void setUser(User user) {
        this.user = user;
        setPersetujuanData(pemesananController.getPemesananData());
    }
    
    public void setPersetujuanData(ObservableList<Pemesanan> pemesananData) {
        persetujuanData.clear();
        for (Pemesanan p: pemesananData) {
            Persetujuan persetujuan = p.getPersetujuanQueue().peek();
            if (persetujuan !=null && persetujuan.getUserId()!=null) {
                if (persetujuan.getUserId().equals(user.getId())) {
                    persetujuanData.add(p);
                    System.out.println(persetujuan.getUserId());
                }
            }
        }
        persetujuanTable.setItems(persetujuanData);
    }

    private void enableButtonAction() {
        persetujuanButton.setDisable(false);
    }
        
     @FXML
    private void filterByDivisi() {
        Divisi d = (Divisi) divisiComboBox.getValue();
        ObservableList<Pemesanan> filtered = FXCollections.observableArrayList();
        for(Pemesanan p: persetujuanData) {
            if (p.getDivisi().getNama().equals(d.getNama())) {
                filtered.add(p);
            }
        }
        persetujuanTable.setItems(filtered);
    }
    
    @FXML
    private void filterByAnggaran() {
        Anggaran a = (Anggaran) anggaranComboBox.getValue();
        ObservableList<Pemesanan> filtered = FXCollections.observableArrayList();
        for(Pemesanan p: persetujuanData) {
            if (p.getAnggaran().getNama().equals(a.getNama())) {
                filtered.add(p);
            }
        }
        persetujuanTable.setItems(filtered);
    }
    
    @FXML
    private void persetujuanButtonHandler() {
        int selectedIndex = persetujuanTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            showPersetujuanDialog(persetujuanTable.getItems().get(selectedIndex));
            pemesananController.savePemesananDataToFile();
            refreshTable();
        }
    }
}
