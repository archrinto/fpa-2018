package anggaran.view;

import anggaran.model.UsersWrapper;
import anggaran.model.PemesananListWrapper;
import anggaran.model.User;
import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


public class UserController {
    
    private ObservableList<User> usersData = FXCollections.observableArrayList(); 
    
    public void loadDataFromFile() {

        try {
            File file = new File("user.xml");
            JAXBContext context = JAXBContext.newInstance(UsersWrapper.class);
            Unmarshaller um = context.createUnmarshaller();
            
            UsersWrapper wrapper = (UsersWrapper) um.unmarshal(file);
            
            if (wrapper.getUsers() != null) {
                usersData.clear();
                usersData.addAll(wrapper.getUsers());
            }
                                    
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void saveDataToFile() {
        try {
            File file = new File("user.xml");
            JAXBContext context = JAXBContext.newInstance(UsersWrapper.class);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            UsersWrapper wrapper = new UsersWrapper();

            wrapper.setUsers(usersData);
            m.marshal(wrapper, file);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public ObservableList<User> getUsersData() {
        return usersData;
    }
}
