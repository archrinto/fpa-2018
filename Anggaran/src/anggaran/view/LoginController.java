package anggaran.view;

import anggaran.MainApp;
import anggaran.model.User;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class LoginController {
    private MainApp mainApp;
    
    @FXML
    private AnchorPane container;
    @FXML 
    private TextField emailField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label pesanLabel;
    
    private User user;
    
    public void initialize() {
        pesanLabel.setText("");
    }
    
    public boolean pengecekanLogin() {
        UserController controller = new UserController();
        controller.loadDataFromFile();
        List<User> users = controller.getUsersData();
        
        for (User u: users) {
            if (u.getEmail().equals(emailField.getText()) || u.getUsername().equals(emailField.getText())) {
                if (u.getPassword().equals(passwordField.getText())) {
                    user = u;
                    return true;
                } 
            }
        }
        
        pesanLabel.setText("Email atau Password salah");
        return false;
    }
    
    public void loginButtonHandler() {
        if (pengecekanLogin()) {
            mainApp.initMainLayout(user);
        }
    } 

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    
}
