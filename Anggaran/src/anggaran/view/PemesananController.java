package anggaran.view;

import anggaran.MainApp;
import anggaran.model.Anggaran;
import anggaran.model.Divisi;
import anggaran.view.PemesananEditController;
import anggaran.model.PemesananListWrapper;
import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import anggaran.model.Pemesanan;
import anggaran.model.Persetujuan;
import anggaran.model.User;
import java.time.LocalDate;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.xml.bind.Marshaller;

public class PemesananController {
    @FXML
    private MainApp mainApp;
    @FXML
    private AnchorPane layout;
    @FXML
    private Button addBtn;
    @FXML
    private Button deleteBtn;
    @FXML
    private Button editBtn;
    @FXML
    private TableView<Pemesanan> pemesananTable;
    @FXML
    private TableColumn<Pemesanan, Divisi> divisiColumn;
    @FXML
    private TableColumn<Pemesanan, Anggaran> anggaranColumn;
    @FXML
    private TableColumn<Pemesanan, Number> nominalColumn;
    @FXML
    private TableColumn<Pemesanan, LocalDate> tanggalColumn;
    @FXML
    private TableColumn<Pemesanan, String> statusColumn;
    @FXML
    private ComboBox divisiComboBox;
    @FXML
    private ComboBox anggaranComboBox;
    
    private ObservableList<Pemesanan> pemesananData = FXCollections.observableArrayList();
    private User user;
    
    public void initialize() {
        try {
        loadPemesananDataFromFile();
        pemesananTable.setItems(pemesananData);
        divisiColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().divisiProperty();
        });
        anggaranColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().anggaranProperty();
        });
        nominalColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().nominalProperty();
        });
        tanggalColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().tanggalProperty();
        });
        statusColumn.setCellValueFactory(cellData -> {
            return cellData.getValue().statusProperty();
        });
               
        pemesananTable.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> enableButtonAction());

        DivisiController divisiController = new DivisiController();
        divisiController.loadDivisiDataFromFile();
        divisiComboBox.setItems(divisiController.getDivisiData());
        
        AnggaranController anggaranController = new AnggaranController();
        anggaranController.loadAnggaranDataFromFile();
        anggaranComboBox.setItems(anggaranController.getAnggaranData());
        
        } catch (Exception e) {
            System.out.println(e);
        }
       
    }
    
    private void enableButtonAction() {
        editBtn.setDisable(false);
        deleteBtn.setDisable(false);                
    }
    
    private void disableButtonAction() {
        editBtn.setDisable(true);
        deleteBtn.setDisable(true);                
    }
    
    private void setButtonActionHandler() {
        System.out.println(deleteBtn);
        deleteBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            int selectedIndex = pemesananTable.getSelectionModel().getSelectedIndex();
            if (selectedIndex >= 0) {
                pemesananTable.getItems().remove(selectedIndex);
                if (pemesananTable.getItems().size() == 0) {
                    disableButtonAction();
                }
                savePemesananDataToFile();
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Peringatan");
                alert.setHeaderText("Tidak ada Pemesanan yang dipilih");
                alert.setContentText("Pilih sebuah data Pemesanan dahulu pada tabel");
                alert.showAndWait();
            }
        });
        
        editBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            int selectedIndex = pemesananTable.getSelectionModel().getSelectedIndex();
            if (selectedIndex >= 0) {
                boolean isSaveClicked = showPemesananEditDialog(pemesananTable.getItems().get(selectedIndex));
                if (isSaveClicked) {
                    savePemesananDataToFile();
                }
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Peringatan");
                alert.setHeaderText("Tidak ada Pemesanan yang dipilih");
                alert.setContentText("Pilih sebuah data Pemesanan dahulu pada tabel");
                alert.showAndWait();
            }
        });
        
        addBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            Pemesanan tempPemesanan = new Pemesanan();
            boolean isSaveClicked = showPemesananEditDialog(tempPemesanan);
            if (isSaveClicked) {
                tempPemesanan.getPersetujuanQueue().add(new Persetujuan("bnDhr"));
                tempPemesanan.getPersetujuanQueue().add(new Persetujuan("DrKs"));
                pemesananData.add(tempPemesanan);
                savePemesananDataToFile();
            }
            
        });
    }

    public boolean showPemesananEditDialog(Pemesanan pemesanan) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("PemesananEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            
            PemesananEditController controller = loader.getController();
            
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ubah Pemesanan");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(mainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            
            controller.setDialogStage(dialogStage);
            controller.setPemesanan(pemesanan);
            
            dialogStage.showAndWait();
            
            return controller.isSaveClicked();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void setLayout(AnchorPane layout) {
        try {
        this.layout = layout;
        setButtonActionHandler();
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    public void setUser(User u) {
        this.user = u;
       // filterByDivisi();
    }
    
    
    public void loadPemesananDataFromFile() {

        try {
            File file = new File("pemesanan.xml");
            JAXBContext context = JAXBContext.newInstance(PemesananListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();
            
            PemesananListWrapper wrapper = (PemesananListWrapper) um.unmarshal(file);
            
            if (wrapper.getListPemesanan() != null) {
                pemesananData.clear();
                pemesananData.addAll(wrapper.getListPemesanan());
            }
                       
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void savePemesananDataToFile() {
        try {
            File file = new File("pemesanan.xml");
            JAXBContext context = JAXBContext.newInstance(PemesananListWrapper.class);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            PemesananListWrapper wrapper = new PemesananListWrapper();

            wrapper.setListPemesanan(pemesananData);
            m.marshal(wrapper, file);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public ObservableList getPemesananData() {
        return pemesananData;
        
    }
    
    @FXML
    private void filterByDivisi() {
        ObservableList<Pemesanan> filtered = FXCollections.observableArrayList();

        if (!user.getJabatan().equals("Admin") && !user.getJabatan().equals("Direksi")) {
            divisiComboBox.setDisable(true);
            String divisiId = user.getDivisiId();
            for(Pemesanan p: pemesananData) {
                if (p.getDivisi().getId().equals(divisiId)) {
                    filtered.add(p);
                }
            }
            pemesananTable.setItems(filtered);
        } else {
            Divisi d = (Divisi) divisiComboBox.getValue();
            System.out.println(d);
            if (d != null) {
                for(Pemesanan p: pemesananData) {
                    if (p.getDivisi().getId().equals(d.getId())) {
                        filtered.add(p);
                    }
                }
            }
            pemesananTable.setItems(filtered);
        }
    }
    
    @FXML
    private void filterByAnggaran() {
        Anggaran a = (Anggaran) anggaranComboBox.getValue();
        ObservableList<Pemesanan> filtered = FXCollections.observableArrayList();
        for(Pemesanan p: pemesananData) {
            if (p.getAnggaran().getNama().equals(a.getNama())) {
                filtered.add(p);
            }
        }
        pemesananTable.setItems(filtered);
    }
  
}
