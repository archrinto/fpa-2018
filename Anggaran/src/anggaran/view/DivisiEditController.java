
package anggaran.view;

import javafx.fxml.FXML;
import anggaran.model.Divisi;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DivisiEditController {
    @FXML
    private TextField namaField;
    @FXML
    private TextArea deskripsiField;
    @FXML
    private Stage dialogStage;
    @FXML
    private Divisi divisi;
    
    private boolean saveClicked = false;
    
    @FXML
    private void initialize() {}
    
    public void setDialogStage(Stage dialog) {
        dialogStage = dialog;
    }
    
    public void setDivisi(Divisi divisi) {
        this.divisi = divisi;
        
        namaField.setText(divisi.getNama());
        deskripsiField.setText(divisi.getDeskripsi());
    }
    
    public void cancelHandler() {
        dialogStage.close();
    }
    
    public void saveHandler() {
        divisi.setNama(namaField.getText());
        divisi.setDeskripsi(deskripsiField.getText());
        saveClicked = true;
        dialogStage.close();
    }
    
    public boolean isSaveClicked() {
        return saveClicked;
    }
    
    
}
