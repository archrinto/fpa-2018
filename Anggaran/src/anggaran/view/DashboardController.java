/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anggaran.view;
import anggaran.model.Anggaran;
import anggaran.model.Frekuensi;
import anggaran.model.Pemesanan;
import anggaran.util.Statistik;
import java.io.File;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.prefs.Preferences;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;


public class DashboardController {
    
    @FXML 
    private Label meanLabel;
    @FXML 
    private Label medianLabel;
    @FXML 
    private Label modusLabel;
    
    @FXML 
    private TableView<Frekuensi> frekuensiAnggaranTable;
   
    @FXML 
    private TableColumn<Frekuensi, Number> frekuensiColumn;
    
    @FXML
    private TableColumn<Frekuensi, String> intervalColumn;
    
    @FXML
    private TableColumn rerataColumn;
    
    @FXML
    private BarChart<String, Integer> barChart;

    @FXML
    private CategoryAxis xAxis;
    
    @FXML
    private AnchorPane container;
    
    @FXML 
    private ScrollPane containerScroll;

    private ObservableList<String> monthNames = FXCollections.observableArrayList();
    
    private ObservableList<Frekuensi> frekuensiData= FXCollections.observableArrayList();
    
    private Statistik statistik;
   
    
    @FXML
    private void initialize() {
        statistik = new Statistik();
        container.setPrefHeight(455.0);
        containerScroll.setPrefHeight(455.0);
        PemesananController pemesananController = new PemesananController();
        pemesananController.loadPemesananDataFromFile();
        List<Pemesanan> listPemesanan = pemesananController.getPemesananData();
        
        String[] months = DateFormatSymbols.getInstance(Locale.ENGLISH).getMonths();
        monthNames.addAll(Arrays.asList(months));
        xAxis.setCategories(monthNames);
        setPemesanan(listPemesanan);
            
        frekuensiAnggaranTable.setItems(frekuensiData);
        
        frekuensiColumn.setCellValueFactory(cellData -> cellData.getValue().frekuensiProperty());
        intervalColumn.setCellValueFactory(cellData -> cellData.getValue().intervalProperty());
       
        
        statistik.setListInterval(1, 10000000, 2000000);
        List<Double> score = new ArrayList<>();
        for(Pemesanan p: listPemesanan) {
            score.add(new Double(p.getNominal().toString()));
        }
        statistik.setListScore(score);
        System.out.println(statistik.getListFrekuensi());
        System.out.println(statistik.getListInterval());
        meanLabel.setText(Double.toString(statistik.getMean()));
        medianLabel.setText(Double.toString(statistik.getMedian()));
        modusLabel.setText(Double.toString(statistik.getModus()));
        distribusiFrekuensiAnggaran();
    }
    
        public void distribusiFrekuensiAnggaran(){
                   for (int i=0; i<statistik.getListInterval().size(); i++){
                      Frekuensi f = new Frekuensi(statistik.getListInterval().get(i)[0], statistik.getListInterval().get(i)[1]);
                      f.setFrekuensi(statistik.getListFrekuensi().get(i));
                      frekuensiData.add(f);
                   }
            
        
        }
        
//        public void hitungMean(){
//            ArrayList<Double> Xi= new ArrayList<Double>(); 
//            ArrayList<Double> Xifi= new ArrayList<Double>(); 
//            double totalFrekuensi = 0,totalXifi= 0;
//               for (Frekuensi f  :frekuensiData   ){
//                   double x = ((double)f.getBatasBawah()+(double) f.getBatasAtas()) /2;
//                    Xi.add(x);
//                    Xifi.add(f.getFrekuensi()*x);
//                    totalFrekuensi += (double) f.getFrekuensi();
//                    totalXifi += f.getFrekuensi()*x; 
//               }
//               double mean = totalXifi/totalFrekuensi;
//               meanLabel.setText(Double.toString(mean));
//           
//        }

        public void setPemesanan(List<Pemesanan> listPemesanan) {
            System.out.println(listPemesanan.size());
            int[] nominalCounter = new int[12];
            for (Pemesanan p : listPemesanan) {
                int month = p.getTanggal().getMonthValue() - 1;
                nominalCounter[month] += p.getNominal();
            }
            
            XYChart.Series<String, Integer> series = new XYChart.Series<>();

            for (int i = 0; i < nominalCounter.length; i++) {
                series.getData().add(new XYChart.Data<>(monthNames.get(i), nominalCounter[i]));
            }

            barChart.getData().add(series);
        }
        
        
        
        
        }
        
