package anggaran.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import anggaran.MainApp;
import anggaran.model.User;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class MainController {
    @FXML
    private AnchorPane container;
    @FXML
    private GridPane menuGridPane;
    @FXML
    private MenuButton userLoginMenuButton;
    @FXML
    private Button dashboardButton;
    @FXML
    private Button pemesananButton;
    @FXML
    private Button anggaranButton;
    @FXML
    private Button divisiButton;
    @FXML
    private Button persetujuanButton;
    @FXML
    private Scene scene;
    @FXML
    private MainApp mainApp;
    @FXML
    private final Image iconDashboard = new Image(getClass().getResourceAsStream("image/dashboard.png"));
    private final Image iconDashboardActive = new Image(getClass().getResourceAsStream("image/dashboard-blue.png"));
    private final Image iconPemesanan = new Image(getClass().getResourceAsStream("image/book.png"));
    private final Image iconPemesananActive = new Image(getClass().getResourceAsStream("image/book-blue.png"));
    private final Image iconAnggaran = new Image(getClass().getResourceAsStream("image/ticket.png"));
    private final Image iconAnggaranActive = new Image(getClass().getResourceAsStream("image/ticket-blue.png"));
    private final Image iconDivisi = new Image(getClass().getResourceAsStream("image/sitemap.png"));
    private final Image iconDivisiActive = new Image(getClass().getResourceAsStream("image/sitemap-blue.png"));
    private final Image iconCheck = new Image(getClass().getResourceAsStream("image/check.png"));
    private final Image iconCheckActive = new Image(getClass().getResourceAsStream("image/check-blue.png"));
    
    private User user;
    
    public MainController() {
    }
     
    public void initial() {
        dashboardButton = (Button)scene.lookup("#dashboardBtn");
        pemesananButton = (Button)scene.lookup("#pemesananBtn");
        anggaranButton = (Button)scene.lookup("#anggaranBtn");
        divisiButton = (Button)scene.lookup("#divisiBtn");
        persetujuanButton = (Button)scene.lookup("#persetujuanBtn");

        setDashboardButtonEventHandler();
        setPemesananButtonEventHandler();
        setAnggaranButtonEventHandler();
        setDivisiButtonEventHandler();
        setPersetujuanButtonEventHandler();
        renderSideMenu();
        loadDefaultContent();
    }
    
    private void loadDefaultContent() {
        MouseEvent mouseEvent = new MouseEvent(MouseEvent.MOUSE_CLICKED, 1, 2, 3, 4, MouseButton.PRIMARY, 5, true, true, true, true, true, true, true, true, true, true, null);
        dashboardButton.fireEvent(mouseEvent);
    }
 
    
    private void setAllButtonNotActive() {
        if (dashboardButton.getStyleClass().remove("active")) {
            ImageView iconView = (ImageView) dashboardButton.lookup(".side_menu_icon");
            iconView.setImage(iconDashboard);
        }
        if (pemesananButton.getStyleClass().remove("active")) {
            ImageView iconView = (ImageView) pemesananButton.lookup(".side_menu_icon");
            iconView.setImage(iconPemesanan);
        }
        if (anggaranButton.getStyleClass().remove("active")) {
            ImageView iconView = (ImageView) anggaranButton.lookup(".side_menu_icon");
            iconView.setImage(iconAnggaran);
        }
        if (divisiButton.getStyleClass().remove("active")) {
            ImageView iconView = (ImageView) divisiButton.lookup(".side_menu_icon");
            iconView.setImage(iconDivisi);
        }
        if (persetujuanButton.getStyleClass().remove("active")) {
            ImageView iconView = (ImageView) divisiButton.lookup(".side_menu_icon");
            iconView.setImage(iconDivisi);
        }
    }

    private void setAnggaranButtonEventHandler() {   
        anggaranButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            if (!anggaranButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) anggaranButton.lookup(".side_menu_icon");
                iconView.setImage(iconAnggaranActive);
            }
        });
        
        anggaranButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            if (!anggaranButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) anggaranButton.lookup(".side_menu_icon");
                iconView.setImage(iconAnggaran);
            }
        });
        
        anggaranButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            if (!anggaranButton.getStyleClass().contains("active")) {
                ImageView iconView = (ImageView) anggaranButton.lookup(".side_menu_icon");
                
                setAllButtonNotActive();
                iconView.setImage(iconAnggaranActive);
                anggaranButton.getStyleClass().add("active");
                
                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("Anggaran.fxml"));
                    AnchorPane content = (AnchorPane) loader.load();
                    AnggaranController controller = loader.getController();
                    
                    controller.setLayout(content);
                    controller.setMainApp(mainApp);
                    setMainContent("Anggaran", content);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void setPemesananButtonEventHandler() {
        pemesananButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            if (!pemesananButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) pemesananButton.lookup(".side_menu_icon");
                iconView.setImage(iconPemesananActive);
            }
        });
        
        pemesananButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            if (!pemesananButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) pemesananButton.lookup(".side_menu_icon");
                iconView.setImage(iconPemesanan);
            }
        });
        
        pemesananButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            if (!pemesananButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) pemesananButton.lookup(".side_menu_icon");
                
                setAllButtonNotActive();
                iconView.setImage(iconPemesananActive);
                pemesananButton.getStyleClass().add("active");
                
                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("Pemesanan.fxml"));
                    AnchorPane content = (AnchorPane) loader.load();
                    PemesananController controller = loader.getController();
                    
                    controller.setLayout(content);
                    controller.setMainApp(mainApp);
                    controller.setUser(user);
                    setMainContent("Pemesanan", content);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void setDashboardButtonEventHandler() {
        dashboardButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            if (!dashboardButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) dashboardButton.lookup(".side_menu_icon");
                iconView.setImage(iconDashboardActive);
            }
        });
        
        dashboardButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            if (!dashboardButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) dashboardButton.lookup(".side_menu_icon");
                iconView.setImage(iconDashboard);
            }
        });
        
        dashboardButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            if (!dashboardButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) dashboardButton.lookup(".side_menu_icon");
                
                setAllButtonNotActive();
                iconView.setImage(iconDashboardActive);
                dashboardButton.getStyleClass().add("active");
                
                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("Dashboard.fxml"));
                    AnchorPane content = (AnchorPane) loader.load();
                    //content.setPrefHeight(465.0);
                    setMainContent("Dashboard", content);
   
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void setDivisiButtonEventHandler() {
        divisiButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            if (!divisiButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) divisiButton.lookup(".side_menu_icon");
                iconView.setImage(iconDivisiActive);
            }
        });
        
        divisiButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            if (!divisiButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) divisiButton.lookup(".side_menu_icon");
                iconView.setImage(iconDivisi);
            }
        });
        
        divisiButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            if (!divisiButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) divisiButton.lookup(".side_menu_icon");
      
                setAllButtonNotActive();
                iconView.setImage(iconDivisiActive);
                divisiButton.getStyleClass().add("active");

                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("Divisi.fxml"));
                    AnchorPane content = (AnchorPane) loader.load();
                    DivisiController controller = loader.getController();
                    
                    controller.setLayout(content);
                    controller.setMainApp(mainApp);
                    setMainContent("Divisi", content);
                    
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
               
            }
        });
    }

    private void setMainContent(String t, AnchorPane p) {
        Label title = (Label) scene.lookup("#pageTitleLabel");
        BorderPane root = (BorderPane) scene.getRoot();
        
        title.setText(t);
        root.setCenter(p);
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        this.scene = this.mainApp.getPrimaryStage().getScene();
    }
    
    public void setUser(User user) {
        this.user = user;
        userLoginMenuButton.setText(user.getNama());
    }
    
    @FXML
    private void userLogout() {
        mainApp.loginLayout();
    }
    
    private void renderSideMenu() {
        ObservableList<Node> listMenu = FXCollections.observableArrayList(menuGridPane.getChildren());
        int nAdded = 0;
        
        if (user.getJabatan().equals("Admin")) {
            //menuGridPane.getChildren().clear();

            /*for (int i=0; i < listMenu.size(); i++) {
                if (listMenu.get(i).getId().equals("anggaranBtn")) {
                    menuGridPane.add(listMenu.get(i), 0, nAdded);
                    nAdded++;
                }
                if (listMenu.get(i).getId().equals("divisiBtn")) {
                    menuGridPane.add(listMenu.get(i), 0, nAdded);
                    nAdded++;
                }
            }*/
        } else if (user.getJabatan().equals("Divisi")) {
            menuGridPane.getChildren().clear();
            
            for (int i=0; i < listMenu.size(); i++) {
                String id = listMenu.get(i).getId();
                if (id.equals("dashboardBtn")
                    || id.equals("pemesananBtn")) {
                    menuGridPane.add(listMenu.get(i), 0, nAdded);
                    nAdded++;
                }
            }
        } else if (user.getJabatan().equals("Direksi")) {
            menuGridPane.getChildren().clear();

            for (int i=0; i < listMenu.size(); i++) {
                String id = listMenu.get(i).getId();
                if (id.equals("persetujuanBtn")
                    || id.equals("dashboardBtn")
                    || id.equals("pemesananBtn")) {
                    menuGridPane.add(listMenu.get(i), 0, nAdded);
                    nAdded++;
                }
            }
        } else if (user.getJabatan().equals("Bendahara")) {
            menuGridPane.getChildren().clear();

            for (int i=0; i < listMenu.size(); i++) {
                String id = listMenu.get(i).getId();
                if (id.equals("persetujuanBtn")
                    || id.equals("dashboardBtn")
                    || id.equals("pemesananBtn")) {
                    menuGridPane.add(listMenu.get(i), 0, nAdded);
                    nAdded++;
                }
            }
        }
        
        
    }

    private void setPersetujuanButtonEventHandler() {
        persetujuanButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            if (!persetujuanButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) persetujuanButton.lookup(".side_menu_icon");
                iconView.setImage(iconCheckActive);
            }
        });
        
        persetujuanButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            if (!persetujuanButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) persetujuanButton.lookup(".side_menu_icon");
                iconView.setImage(iconCheck);
            }
        });
        
        persetujuanButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            if (!persetujuanButton.getStyleClass().contains("active")){
                ImageView iconView = (ImageView) persetujuanButton.lookup(".side_menu_icon");
      
                setAllButtonNotActive();
                iconView.setImage(iconCheckActive);
                persetujuanButton.getStyleClass().add("active");

                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("Persetujuan.fxml"));
                    AnchorPane content = (AnchorPane) loader.load();
                    PersetujuanController controller = loader.getController();
                    
                    controller.setMainApp(mainApp);
                    controller.setUser(user);
                    setMainContent("Persetujuan", content);
                    
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
               
            }
        });
    }
    
}
