package anggaran.view;

import anggaran.MainApp;
import anggaran.view.DivisiEditController;
import anggaran.model.DivisiListWrapper;
import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import anggaran.model.Divisi;
import java.util.prefs.Preferences;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.xml.bind.Marshaller;

public class DivisiController {
    @FXML
    private MainApp mainApp;
    @FXML
    private AnchorPane layout;
    @FXML
    private TableView<Divisi> divisiTable;
    @FXML
    private TableColumn<Divisi, String> namaColumn;
    @FXML
    private TableColumn<Divisi, String> deskripsiColumn;
    
    private ObservableList<Divisi> divisiData = FXCollections.observableArrayList();
    
    public void initialize() {
        loadDivisiDataFromFile();
        divisiTable.setItems(divisiData);
        namaColumn.setCellValueFactory(cellData -> cellData.getValue().namaProperty());
        deskripsiColumn.setCellValueFactory(cellData -> cellData.getValue().deskripsiProperty());
        
        divisiTable.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> enableButtonAction());        
    }
    
    private void enableButtonAction() {
        layout.lookup("#editButton").setDisable(false);
        layout.lookup("#deleteButton").setDisable(false);                
    }
    
    private void disbaleButtonAction() {
        layout.lookup("#editButton").setDisable(true);
        layout.lookup("#deleteButton").setDisable(true);                
    }
    
    private void setButtonActionHandler() {
        layout.lookup("#deleteButton").addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            int selectedIndex = divisiTable.getSelectionModel().getSelectedIndex();
            System.out.println(selectedIndex);
            if (selectedIndex >= 0) {
                divisiTable.getItems().remove(selectedIndex);
                if (divisiTable.getItems().size() == 0) {
                    disbaleButtonAction();
                }
                saveDivisiDataToFile();
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Peringatan");
                alert.setHeaderText("Tidak ada Divisi yang dipilih");
                alert.setContentText("Pilih sebuah data Divisi dahulu pada tabel");
                alert.showAndWait();
            }
        });
        
        layout.lookup("#editButton").addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            int selectedIndex = divisiTable.getSelectionModel().getSelectedIndex();
            if (selectedIndex >= 0) {
                boolean isSaveClicked = showDivisiEditDialog(divisiTable.getItems().get(selectedIndex));
                if (isSaveClicked) {
                    saveDivisiDataToFile();
                }
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Peringatan");
                alert.setHeaderText("Tidak ada Divisi yang dipilih");
                alert.setContentText("Pilih sebuah data Divisi dahulu pada tabel");
                alert.showAndWait();
            }
        });
        
        layout.lookup("#addButton").addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            Divisi tempDivisi = new Divisi();
            boolean isSaveClicked = showDivisiEditDialog(tempDivisi);
            if (isSaveClicked) {
                divisiData.add(tempDivisi);
                saveDivisiDataToFile();
            }
            
        });
    }
    
    public boolean showDivisiEditDialog(Divisi divisi) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("DivisiEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            
            DivisiEditController controller = loader.getController();
            
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ubah Divisi");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(mainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            
            controller.setDialogStage(dialogStage);
            controller.setDivisi(divisi);
            
            dialogStage.showAndWait();
            
            return controller.isSaveClicked();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void setLayout(AnchorPane layout) {
        this.layout = layout;
        setButtonActionHandler();
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    public File getDivisiFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(DivisiController.class);
        String filePath = prefs.get("filepath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }
    
    public void setDivisiFilePath(File f) {
        Preferences prefs = Preferences.userNodeForPackage(DivisiController.class);
        if (f != null) {
            prefs.put("filePath", f.getPath());
        } else {
            prefs.remove("filePath");
        }
    }
    
    public void loadDivisiDataFromFile() {
        try {
            File file = new File("divisi.xml");
            JAXBContext context = JAXBContext.newInstance(DivisiListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();
            
            DivisiListWrapper wrapper = (DivisiListWrapper) um.unmarshal(file);
            
            if (wrapper.getListDivisi() != null) {
                divisiData.clear();
                divisiData.addAll(wrapper.getListDivisi());
            }
            
            setDivisiFilePath(file);
            
        } catch (Exception e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not load data");
            alert.showAndWait();
        }
    }
    
    public void saveDivisiDataToFile() {
        try {
            File file = new File("divisi.xml");
            JAXBContext context = JAXBContext.newInstance(DivisiListWrapper.class);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            DivisiListWrapper wrapper = new DivisiListWrapper();

            wrapper.setListDivisi(divisiData);
            m.marshal(wrapper, file);

            setDivisiFilePath(file);
        } catch (Exception e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not save data");
            alert.showAndWait();
        }
    }
    
    public ObservableList<Divisi> getDivisiData() {
        return divisiData;
    }
}
