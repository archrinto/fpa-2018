package anggaran.view;

import anggaran.MainApp;
import anggaran.view.AnggaranEditController;
import anggaran.model.AnggaranListWrapper;
import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import anggaran.model.Anggaran;
import java.util.prefs.Preferences;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.xml.bind.Marshaller;

public class AnggaranController {
    @FXML
    private MainApp mainApp;
    @FXML
    private AnchorPane layout;
    @FXML
    private TableView<Anggaran> anggaranTable;
    @FXML
    private TableColumn<Anggaran, String> namaColumn;
    @FXML
    private TableColumn<Anggaran, String> deskripsiColumn;
    
    private ObservableList<Anggaran> anggaranData = FXCollections.observableArrayList();
    
    public void initialize() {
        loadAnggaranDataFromFile();
        anggaranTable.setItems(anggaranData);
        namaColumn.setCellValueFactory(cellData -> cellData.getValue().namaProperty());
        deskripsiColumn.setCellValueFactory(cellData -> cellData.getValue().deskripsiProperty());
        
        anggaranTable.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> enableButtonAction());        
    }
    
    private void enableButtonAction() {
        layout.lookup("#editButton").setDisable(false);
        layout.lookup("#deleteButton").setDisable(false);                
    }
    
    private void setButtonActionHandler() {
        layout.lookup("#deleteButton").addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            int selectedIndex = anggaranTable.getSelectionModel().getSelectedIndex();
            System.out.println(selectedIndex);
            if (selectedIndex >= 0) {
                anggaranTable.getItems().remove(selectedIndex);
                saveAnggaranDataToFile();
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Peringatan");
                alert.setHeaderText("Tidak ada Anggaran yang dipilih");
                alert.setContentText("Pilih sebuah data Anggaran dahulu pada tabel");
                alert.showAndWait();
            }
        });
        
        layout.lookup("#editButton").addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            int selectedIndex = anggaranTable.getSelectionModel().getSelectedIndex();
            if (selectedIndex >= 0) {
                boolean isSaveClicked = showAnggaranEditDialog(anggaranTable.getItems().get(selectedIndex));
                if (isSaveClicked) {
                    saveAnggaranDataToFile();
                }
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Peringatan");
                alert.setHeaderText("Tidak ada Anggaran yang dipilih");
                alert.setContentText("Pilih sebuah data Anggaran dahulu pada tabel");
                alert.showAndWait();
            }
        });
        
        layout.lookup("#addButton").addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
            Anggaran tempAnggaran = new Anggaran();
            boolean isSaveClicked = showAnggaranEditDialog(tempAnggaran);
            if (isSaveClicked) {
                anggaranData.add(tempAnggaran);
                saveAnggaranDataToFile();
            }
            
        });
    }
    
    public boolean showAnggaranEditDialog(Anggaran anggaran) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("AnggaranEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            
            AnggaranEditController controller = loader.getController();
            
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ubah Anggaran");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(mainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            
            controller.setDialogStage(dialogStage);
            controller.setAnggaran(anggaran);
            
            dialogStage.showAndWait();
            
            return controller.isSaveClicked();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void setLayout(AnchorPane layout) {
        this.layout = layout;
        setButtonActionHandler();
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    public File getAnggaranFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(AnggaranController.class);
        String filePath = prefs.get("filepath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }
    
    public void setAnggaranFilePath(File f) {
        Preferences prefs = Preferences.userNodeForPackage(AnggaranController.class);
        if (f != null) {
            prefs.put("filePath", f.getPath());
        } else {
            prefs.remove("filePath");
        }
    }
    
    public void loadAnggaranDataFromFile() {
        try {
            File file = new File("anggaran.xml");
            JAXBContext context = JAXBContext.newInstance(AnggaranListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();
            
            AnggaranListWrapper wrapper = (AnggaranListWrapper) um.unmarshal(file);
            
            if (wrapper.getListAnggaran() != null) {
                anggaranData.clear();
                anggaranData.addAll(wrapper.getListAnggaran());
            }
            
            setAnggaranFilePath(file);
            
        } catch (Exception e) {
            System.out.println(e);
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not load data");
            alert.showAndWait();
        }
    }
    
    public void saveAnggaranDataToFile() {
        try {
            File file = new File("anggaran.xml");
            JAXBContext context = JAXBContext.newInstance(AnggaranListWrapper.class);
            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            AnggaranListWrapper wrapper = new AnggaranListWrapper();

            wrapper.setListAnggaran(anggaranData);
            m.marshal(wrapper, file);

            setAnggaranFilePath(file);
        } catch (Exception e) {
            System.out.println(e);
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not save data");
            alert.showAndWait();
        }
    }
    
    public ObservableList<Anggaran> getAnggaranData() {
        return anggaranData;
    }
}
