
package anggaran.view;

import anggaran.model.Pemesanan;
import anggaran.model.Persetujuan;
import java.util.Date;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class PersetujuanDialogController {
    @FXML
    private Label divisiLabel;
    @FXML
    private Label anggaranLabel;
    @FXML
    private Label nominalLabel;
    @FXML
    private Label tanggalLabel;
    
    private Pemesanan pemesanan;
    private Stage dialogStage;
    
    public void initialize() {
        
    }
    
    public void setPemesanan(Pemesanan p) {
        pemesanan = p;
        divisiLabel.setText(p.getDivisi().getNama());
        anggaranLabel.setText(p.getAnggaran().getNama());
        nominalLabel.setText(Integer.toString(p.getNominal()));
        tanggalLabel.setText(p.getTanggal().toString());
    }
    
    public void setDialogStage(Stage d) {
        dialogStage = d;
    }
    
    @FXML
    private void handleMenyetujui() {
        persetujuan("Disetujui");
        dialogStage.close();
    }
    
    @FXML
    private void handleMenolak() {
        persetujuan("Ditolak");
        dialogStage.close();
    }
    
    private void persetujuan(String status) {
        Persetujuan p = pemesanan.getPersetujuanQueue().remove();
        p.setStatus(status);
        if (pemesanan.getPersetujuanQueue().peek() == null || status.equals("Ditolak")) {
            pemesanan.setStatus(status);
        }
        p.setWaktu(new Date());
        pemesanan.getPersetujuanList().add(p);
    }
}
