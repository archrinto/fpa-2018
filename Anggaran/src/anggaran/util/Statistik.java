/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anggaran.util;

import static java.lang.Math.abs;
import static java.lang.Math.round;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author July Arfianto (julyarief@gmail.com)
 */
public class Statistik {

    private int n;
    private double lowest;
    private double highest;
    private double interval;
    private List<double[]> listInterval;
    private List<Double> listScore;
    private List<Double> listFrekuensi;
    private List<Double> listFrekuensiK;
    private List<Double> listRerataKelas;
    
    public Statistik() {
        n = 0;
        listInterval = new ArrayList<>();
        listFrekuensi  = new ArrayList<>();
        listFrekuensiK  = new ArrayList<>();
        listRerataKelas  = new ArrayList<>();
    }
        
    public void setListInterval(double lowest, double highest, double interval) {
        this.highest = highest;
        this.lowest = lowest;
        this.interval = interval;
        for (double i = lowest; i <= highest; i = i+interval) {
            listInterval.add(new double[]{i, i+interval-1});
            listFrekuensi.add(0.0);
            listFrekuensiK.add(0.0);
            listRerataKelas.add(0.0);
        }
    }
    
    private void generateFrekuensi() {
        for (Double score: listScore) {
            for (int i=0; i<listInterval.size(); i++) {
                if (score >= listInterval.get(i)[0] && score <= listInterval.get(i)[1]) {
                    listFrekuensi.set(i, listFrekuensi.get(i)+1);
                    n++;
                }
            }
        }
    }
    
    private void generateFrekuensiK() {
        double temp = 0;
        for (int i=0; i< listFrekuensi.size(); i++) {
            listFrekuensiK.set(i, temp+listFrekuensi.get(i));
            temp = listFrekuensiK.get(i);
        }
    }
    
    private void generateRerataKelas() {
        for (int i=0; i< listRerataKelas.size(); i++) {
            double Tb = listInterval.get(i)[0] - 0.5;
            double Ta = listInterval.get(i)[1] + 0.5;
            listRerataKelas.set(i, (Ta+Tb)/2);
        }
    }

    public List<Double> getListFrekuensi() {        
        return listFrekuensi;
    }
    
    public List<Double> getListRerataKelas() {        
        return listRerataKelas;
    }

    public void setListFrekuensi(List<Double> listFrekuensi) {
        this.listFrekuensi = listFrekuensi;
    }
    
    
    public List<Double> getListScore() {
        return listScore;
    }

    public void setListScore(List<Double> listScore) {
        this.listScore = listScore;
        generateFrekuensi();
        generateFrekuensiK();
        generateRerataKelas();
    }
       
    
    public List<double[]> getListInterval() {
        return listInterval;
    }
    
    private int getIndexMedian(double nn) {
        for (int i=0; i < listFrekuensiK.size(); i++) {
            if (nn <= listFrekuensiK.get(i)) {
                return i;
            }
        }
        return -1;
    }
    
    public double getMean() {
        double rerata = 0;
        double totalF = 0;
        double totalXF = 0;
        for (int i=0; i < listFrekuensi.size(); i++) {
            totalF += listFrekuensi.get(i);
            totalXF += listRerataKelas.get(i) * listFrekuensi.get(i);
        }
        rerata = totalXF / totalF;
        return pembulatan(rerata, 2);
    }

    public double getMedian() {
        double nn = n / 2;
        double Fkk = 0;
        int indexMedian = getIndexMedian(nn);
        double Tb = listInterval.get(indexMedian)[0] - 0.5;
        double p = interval;
        if (indexMedian-1 != -1) {
            Fkk = listFrekuensiK.get(indexMedian-1);
        }
        double Fi = listFrekuensi.get(indexMedian);
        double Me = Tb + ((nn - Fkk)/Fi) * p;
       
        return pembulatan(Me, 2);
    }
    
    public double getModus() {
        double nn = n / 2;
        double d1=0,d2=0;
        int indexMedian = getIndexMedian(nn);
        double Tb = listInterval.get(indexMedian)[0] - 0.5;
        if (indexMedian-1 != -1) {
            d1 = abs(listFrekuensi.get(indexMedian) - listFrekuensi.get(indexMedian-1));
            d2 = abs(listFrekuensi.get(indexMedian) - listFrekuensi.get(indexMedian+1));
        } else {
            d1 = abs(listFrekuensi.get(indexMedian) - 0);
            d2 = abs(listFrekuensi.get(indexMedian) - listFrekuensi.get(indexMedian+1));
        }
        
        double p = interval;
        double Mo = Tb + (d1 / (d1+d2)) * p;
        return pembulatan(Mo,2);
    }
    
    private double pembulatan(double d, int desimal) {
        double temp = Math.pow(10, desimal);
        double y = (double) Math.round(d*temp)/temp;
        return y;
    }
    
   
        
//    public static void main(String[] args) {
//        Statistik statistik = new Statistik();
//        statistik.setListInterval(31, 90, 10);
//        List<Double> listScore = new ArrayList<>();
//        double[] score = {32,33,34,42,42,42,42,42,56,56,56,56,56,56,56,56,56,56,63,63,63,63,63,63,63,63,63,63,63,78,78,78,78,78,78,78,78,84,84,84};
//        
//        for(double s: score) {
//            listScore.add(s);
//        }
//        statistik.setListScore(listScore);
//        
//        System.out.println(statistik.getMean());
//        System.out.println(statistik.getMedian());
//        System.out.println(statistik.getModus());
//    }
    
   
}
