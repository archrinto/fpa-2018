package anggaran.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "listAnggaran")
public class AnggaranListWrapper {
    private List<Anggaran> listAnggaran;
    
    @XmlElement(name = "anggaran")
    public List<Anggaran> getListAnggaran() {
        return listAnggaran;
    }
    
    public void setListAnggaran(List<Anggaran> list) {
        listAnggaran = list;
    }
}
