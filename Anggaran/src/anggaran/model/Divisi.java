
package anggaran.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Divisi {
    private String id;
    private final StringProperty nama;
    private final StringProperty deskripsi;
    
    public Divisi() {
        this(null, null);
    }

    public Divisi(String n, String k) {
        nama = new SimpleStringProperty(n);
        deskripsi = new SimpleStringProperty(k);
    }

    public void setNama(String n) {
        nama.set(n);
    }

    public void setDeskripsi(String k) {
        deskripsi.set(k);
    }

    public String getNama() {
        return nama.get();
    }

    public String getDeskripsi() {
        return deskripsi.get();
    }
    
    public StringProperty namaProperty() {
        return nama;
    }

    public StringProperty deskripsiProperty() {
        return deskripsi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
       
    @Override
    public String toString() {
        return nama.get();
    }
}