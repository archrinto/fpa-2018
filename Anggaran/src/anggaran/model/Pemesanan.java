package anggaran.model;

import anggaran.util.LocalDateAdapter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

public class Pemesanan {
    
    private final ObjectProperty<Anggaran> anggaran;
    private final ObjectProperty<Divisi> divisi;
    private final StringProperty status;
    private final IntegerProperty nominal;
    private final ObjectProperty<LocalDate> tanggal;
    private Queue<Persetujuan> persetujuanQueue;
    private List<Persetujuan> persetujuanList;
    
    public Pemesanan() {
        anggaran = new SimpleObjectProperty(null);
        divisi = new SimpleObjectProperty(null);
        status = new SimpleStringProperty(null);
        nominal = new SimpleIntegerProperty(0);
        tanggal = new SimpleObjectProperty(null);
        
        persetujuanQueue = new LinkedList<Persetujuan>();
        persetujuanList = new ArrayList<Persetujuan>();
    }

    public ObjectProperty<Anggaran> anggaranProperty() {
        return anggaran;
    }

    public ObjectProperty<Divisi> divisiProperty() {
        return divisi;
    }

    public StringProperty statusProperty() {
        return status;
    }

    public IntegerProperty nominalProperty() {
        return nominal;
    }

    public ObjectProperty<LocalDate> tanggalProperty() {
        return tanggal;
    }
    
    public void setAnggaran(Anggaran a) {
        anggaran.set(a);
    }
    
    public Anggaran getAnggaran() {
        return anggaran.get();
    }
    
    public void setDivisi(Divisi d) {
        divisi.set(d);
    }
    
     public Divisi getDivisi() {
        return divisi.get();
    }
    
    public void setStatus(String s) {
        status.set(s);
    }
    
    public String getStatus() {
        return status.get();
    }
    
    public void setNominal(Integer n) {
        nominal.set(n);
    } 
    
    public Integer getNominal() {
        return nominal.get();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getTanggal() {
        return tanggal.get();
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal.set(tanggal);
    }

    public Queue<Persetujuan> getPersetujuanQueue() {
        return persetujuanQueue;
    }

    public void setPersetujuanQueue(Queue<Persetujuan> persetujuanQueue) {
        this.persetujuanQueue = persetujuanQueue;
    }

    public List<Persetujuan> getPersetujuanList() {
        return persetujuanList;
    }

    public void setPersetujuanList(List<Persetujuan> persetujuanList) {
        this.persetujuanList = persetujuanList;
    }
        
}
