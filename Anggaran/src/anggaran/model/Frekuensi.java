package anggaran.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class Frekuensi {
    private final  SimpleDoubleProperty batasAtas;
    private final  SimpleDoubleProperty batasBawah;
    private final  SimpleDoubleProperty frekuensi;
    
    public Frekuensi(double b,double a){
        batasAtas = new SimpleDoubleProperty(a);
        batasBawah = new SimpleDoubleProperty(b);
        frekuensi = new SimpleDoubleProperty(0);
    }
    public double getBatasAtas() {
        return batasAtas.get();
    }

    public void setBatasAtas(Double batasAtas) {
        this.batasAtas.set(batasAtas);
    }

    public double getBatasBawah() {
        return batasBawah.get();
    }

    public void setBatasBawah(Double batasBawah) {
        this.batasBawah.set (batasBawah);
    }

    public double getFrekuensi() {
        return frekuensi.get();
    }

    public void setFrekuensi(Double frekuensi) {
        this.frekuensi.set (frekuensi);
    
   
    }
    public SimpleStringProperty intervalProperty() {
        String s = Double.toString(batasBawah.get()) + " - " + Double.toString(batasAtas.get());
        return new SimpleStringProperty(s);
    }
    
    public SimpleDoubleProperty frekuensiProperty(){
        return frekuensi;
    }
    
    
}
