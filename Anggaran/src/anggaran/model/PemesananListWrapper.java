package anggaran.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "listPemesanan")
public class PemesananListWrapper {
    private List<Pemesanan> listPemesanan;
    
    @XmlElement(name = "pemesanan")
    public List<Pemesanan> getListPemesanan() {
        return listPemesanan;
    }

    public void setListPemesanan(List<Pemesanan> listPemesanan) {
        this.listPemesanan = listPemesanan;
    }
        
}
