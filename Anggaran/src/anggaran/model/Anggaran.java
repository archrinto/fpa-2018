
package anggaran.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class Anggaran {
 
    private final SimpleStringProperty nama ;
    private final StringProperty deskripsi;
    
    public Anggaran(){
      this(null);
    }
         
    public Anggaran(String nama) {
        this.nama = new SimpleStringProperty(nama);
        this.deskripsi = new SimpleStringProperty(null);    
    }

    public String getNama() {
        return nama.get();
    }
    
    public StringProperty namaProperty() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama.set(nama);
    }

    public String getDeskripsi() {
        return deskripsi.get();
    }
    public StringProperty deskripsiProperty() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi.set(deskripsi);
    }
    
    @Override
    public String toString() {
        return nama.get();
    }
}

