
package anggaran.model;

import java.util.Date;

public class Persetujuan {
    private String userId;
    private Date waktu;
    private String status;
    private String keterangan;
    
    public Persetujuan() {
        this(null);
    }
    
    public Persetujuan(String userId) {
        this.userId = userId;
        this.status = "Diajukan";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String id) {
        this.userId = id;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

}
