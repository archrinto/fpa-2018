package anggaran.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "listDivisi")
public class DivisiListWrapper {
    private List<Divisi> listDivisi;
    
    @XmlElement(name = "divisi")
    public List<Divisi> getListDivisi() {
        return listDivisi;
    }
    
    public void setListDivisi(List<Divisi> list) {
        listDivisi = list;
    }
}
