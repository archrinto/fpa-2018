package anggaran;

import anggaran.model.User;
import anggaran.view.LoginController;
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import anggaran.view.MainController;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class MainApp extends Application {

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        
        loginLayout();
    }
    
    public void loginLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/Login.fxml"));
            BorderPane layout = (BorderPane) loader.load();
            
            Scene scene = new Scene(layout);
            primaryStage.setScene(scene);
            
            LoginController controller = loader.getController();
            controller.setMainApp(this);
                        
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initMainLayout(User user) {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/Main.fxml"));
            BorderPane mainLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(mainLayout);
            primaryStage.setScene(scene);
            
            MainController controller = loader.getController();
            controller.setMainApp(this);
            controller.setUser(user);
            controller.initial();
            
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}